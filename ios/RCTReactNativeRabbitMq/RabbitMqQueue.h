#import <React/RCTBridgeModule.h>

#import <RMQClient/RMQClient.h>
#import "EventEmitter.h"

@interface RabbitMqQueue : NSObject <RCTBridgeModule>

    - (nonnull id) initWithConfig:(nonnull NSDictionary *)config
                          channel:(nonnull id<RMQChannel>)channel;
    - (BOOL) isConsuming;
    - (void) bind:(nonnull RMQExchange *)exchange routing_key:(NSString *)routing_key;
    - (void) unbind:(nonnull RMQExchange *)exchange routing_key:(NSString *)routing_key;
    - (void) delete;
    - (void) ack: (NSNumber *)deliveryTag;
    - (void) reject:(NSNumber *)deliveryTag requeue:(BOOL)requeue;
    - (void) consume;
    - (void) cancelConsumer;
@end
