package nl.kega.reactnativerabbitmq;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.facebook.react.modules.core.DeviceEventManagerModule;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.GuardedAsyncTask;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.ShutdownListener; 
import com.rabbitmq.client.ShutdownSignalException; 
import com.rabbitmq.client.RecoveryListener;
import com.rabbitmq.client.Recoverable;
import com.rabbitmq.client.RecoverableConnection;
import com.rabbitmq.client.ConfirmListener;

import javax.annotation.Nullable;

class RabbitMqConnection extends ReactContextBaseJavaModule  {

    private ReactApplicationContext context;

    public ReadableMap config;

    private ConnectionFactory factory = null;
    private RecoverableConnection connection;
    private Channel channel;

    private Callback status;

    private ArrayList<RabbitMqQueue> queues = new ArrayList<RabbitMqQueue>();
    private ArrayList<RabbitMqExchange> exchanges = new ArrayList<RabbitMqExchange>(); 

    public RabbitMqConnection(ReactApplicationContext reactContext) {
        super(reactContext);

        this.context = reactContext;

    }

    @Override
    public String getName() {
        return "RabbitMqConnection";
    }

    @ReactMethod
    public void initialize(ReadableMap config) {
        this.config = config;
        
        this.factory = new ConnectionFactory();
        this.factory.setUsername(this.config.getString("username"));
        this.factory.setPassword(this.config.getString("password"));
        this.factory.setVirtualHost(this.config.getString("virtualhost"));
        this.factory.setHost(this.config.getString("host"));
        this.factory.setPort(this.config.getInt("port"));
        this.factory.setAutomaticRecoveryEnabled(true);
        this.factory.setRequestedHeartbeat(10);

        try {
            if (this.config.hasKey("ssl") && this.config.getBoolean("ssl")) {
                this.factory.useSslProtocol();
            }
        } catch(Exception e) {
            Log.e("RabbitMqConnection", e.toString());
        }

    }

    @ReactMethod
    public void status(Callback onStatus) {
        this.status = onStatus;
    }

    @ReactMethod
    public void connect() {

        if (this.connection != null && this.connection.isOpen() && this.channel != null && this.channel.isOpen()) {
            WritableMap event = Arguments.createMap();
            event.putString("name", "connected");

            this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);
        } else {
            try {
                this.connection = (RecoverableConnection)this.factory.newConnection();
            } catch (Exception e){

                WritableMap event = Arguments.createMap();
                event.putString("name", "error");
                event.putString("type", "failedtoconnect");
                event.putString("code", "");
                event.putString("description", e.getMessage());

                this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);

                this.connection = null; 

            }

            if (this.connection != null){
                try {
                    this.connection.addShutdownListener(new ShutdownListener() {
                        @Override
                        public void shutdownCompleted(ShutdownSignalException cause) {
                            Log.e("RabbitMqConnection", "Shutdown signal received " + cause);
                            onClose(cause);
                        }
                    });

                    this.connection.addRecoveryListener(new RecoveryListener() {
                        @Override
                        public void handleRecoveryStarted(Recoverable recoverable) {
                            Log.e("RabbitMqConnection", "RecoveryStarted " + recoverable);
                        }
                        @Override
                        public void handleRecovery(Recoverable recoverable) {
                            Log.e("RabbitMqConnection", "Recoverable " + recoverable);
                            onRecovered();
                        }
                    });

                    this.channel = connection.createChannel();
                    this.channel.basicQos(1);
                    if (this.config.getBoolean("confirmSelect")) {
                        this.channel.confirmSelect();
                    }

                    this.channel.addConfirmListener(new ConfirmListener() {
                        public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                            Log.e("RabbitMqQueue", "Not ack received");
                        }
                        public void handleAck(long deliveryTag, boolean multiple) throws IOException {
                            Log.e("RabbitMqQueue", "Ack received");
                        }
                    });


                    WritableMap event = Arguments.createMap();
                    event.putString("name", "connected");

                    this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);
                } catch (Exception e) {
                    Log.e("RMQConnectionChannel", "Create channel error " + e);
                    e.printStackTrace();

                    WritableMap event = Arguments.createMap();
                    event.putString("name", "error");
                    event.putString("type", "failedtocreatechannel");
                    event.putString("code", "");
                    event.putString("description", e.getMessage());
    
                    this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);
                } 
            }
        }
    }

    @ReactMethod
    public void addQueue(ReadableMap queue_config, ReadableMap arguments) {
        RabbitMqQueue queue = new RabbitMqQueue(this.context, this.channel, queue_config, arguments);
        this.queues.add(queue);

        WritableMap event = Arguments.createMap();
        event.putString("name", "queueCreated");
        event.putString("queue_name", queue.name);

        this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);
    }

    @ReactMethod
    public void consumeAll() {
        for (RabbitMqQueue queue: queues) {
            if(!queue.isConsuming()) {
                queue.startConsuming();
            }
        }
    }

    @ReactMethod
    public void consume(String queue_name) {
        for (RabbitMqQueue queue: queues) {
            if(queue.name.compareTo(queue_name) == 0 && !queue.isConsuming()) {
                queue.startConsuming();
            }
        }
    }

    @ReactMethod
    public void bindQueue(String exchange_name, String queue_name, String routing_key) {
        
        RabbitMqQueue found_queue = null;
        for (RabbitMqQueue queue : queues) {
            if (queue_name.compareTo(queue.name) == 0){
                found_queue = queue;
            }
        }

        RabbitMqExchange found_exchange = null;
        for (RabbitMqExchange exchange : exchanges) {
            if (exchange_name.compareTo(exchange.name) == 0){
                found_exchange = exchange;
            }
        }

        if (!found_queue.equals(null) && !found_exchange.equals(null)){
            found_queue.bind(found_exchange, routing_key);

            WritableMap event = Arguments.createMap();
            event.putString("name", "queueBound");
            event.putString("queue_name", found_queue.name);
            event.putString("exchange_name", found_exchange.name);

            this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);

        }
    }

    @ReactMethod
    public void unbindQueue(String exchange_name, String queue_name, String routing_key) {
        
        RabbitMqQueue found_queue = null;
        for (RabbitMqQueue queue : queues) {
            if (queue_name.compareTo(queue.name) == 0){
                found_queue = queue;
            }
        }

        RabbitMqExchange found_exchange = null;
        for (RabbitMqExchange exchange : exchanges) {
            if (exchange_name.compareTo(exchange.name) == 0){
                found_exchange = exchange;
            }
        }

        if (!found_queue.equals(null) && !found_exchange.equals(null)){
            found_queue.unbind(routing_key);

            WritableMap event = Arguments.createMap();
            event.putString("name", "queueUnbound");
            event.putString("queue_name", found_queue.name);

            this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);

        }
    }

    @ReactMethod
    public void removeQueue(String queue_name) {
        RabbitMqQueue found_queue = null;
        for (RabbitMqQueue queue : queues) {
            if (queue_name.compareTo(queue.name) == 0){
                found_queue = queue;
            }
        }

        if (!found_queue.equals(null)){
            found_queue.delete();

            WritableMap event = Arguments.createMap();
            event.putString("name", "queueRemoved");
            event.putString("queue_name", queue_name);

            this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);

        }
    }

    @ReactMethod
    public void basicAck(String queue_name, Double delivery_tag) {
        RabbitMqQueue found_queue = null;
        for (RabbitMqQueue queue : queues) {
            if (queue_name.compareTo(queue.name) == 0){
                found_queue = queue;
            }
        }

        if (!found_queue.equals(null)){
            long long_delivery_tag = Double.valueOf(delivery_tag).longValue();
            found_queue.basicAck(long_delivery_tag);
        }
    }

    @ReactMethod
    public void basicReject(String queue_name, Double delivery_tag, boolean requeue) {
        RabbitMqQueue found_queue = null;
        for (RabbitMqQueue queue : queues) {
            if (queue_name.compareTo(queue.name) == 0){
                found_queue = queue;
            }
        }

        if (!found_queue.equals(null)){
            long long_delivery_tag = Double.valueOf(delivery_tag).longValue();
            found_queue.basicReject(long_delivery_tag, requeue);
        }
    }

    @ReactMethod
    public void addExchange(ReadableMap exchange_config) {

        RabbitMqExchange exchange = new RabbitMqExchange(this.context, this.channel, exchange_config);

        this.exchanges.add(exchange);

        WritableMap event = Arguments.createMap();
        event.putString("name", "exchangeCreated");
        event.putString("exchange_name", exchange.name);

        this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);
    }

    @ReactMethod
    public void publishToExchange(String message, String exchange_name, String routing_key, ReadableMap message_properties, @Nullable String publishId) {
        for (RabbitMqExchange exchange : exchanges) {
            if (exchange_name.compareTo(exchange.name) == 0) {
                Log.e("RabbitMqConnection", "Exchange publish: " + message);
                exchange.publish(message, routing_key, message_properties, publishId != null ? publishId : "");
            }
		}
    }

    @ReactMethod
    public void deleteExchange(String exchange_name, Boolean if_unused) {
        for (RabbitMqExchange exchange : exchanges) {
            if (exchange_name.compareTo(exchange.name) == 0){
                exchange.delete(if_unused);

                WritableMap event = Arguments.createMap();
                event.putString("name", "exchangeRemoved");
                event.putString("exchange_name", exchange_name);
    
                this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);
    
                return;
            }
		}
    }

    @ReactMethod
    public void close() {
        try {
            
            this.queues = new ArrayList<RabbitMqQueue>();
            this.exchanges = new ArrayList<RabbitMqExchange>(); 
            
            this.channel.close();

            this.connection.close();
        } catch (Exception e){
            Log.e("RabbitMqConnection", "Connection closing error " + e);
            e.printStackTrace();
        } finally { 
            this.connection = null; 
            this.factory = null;
            this.channel = null;
        } 
    }

    private void onClose(ShutdownSignalException cause) { 
        Log.e("RabbitMqConnection", "Closed");

        WritableMap event = Arguments.createMap();
        event.putString("name", "closed");

        this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);
    } 

    private void onRecovered() { 
        Log.e("RabbitMqConnection", "Recovered");

        WritableMap event = Arguments.createMap();
        event.putString("name", "reconnected");

        this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit("RabbitMqConnectionEvent", event);
    } 

    @Override
    public void onCatalystInstanceDestroy() {

        // serialize on the AsyncTask thread, and block
        try {
            new GuardedAsyncTask<Void, Void>(getReactApplicationContext()) {
                @Override
                protected void doInBackgroundGuarded(Void... params) {
                    close();
                }
            }.execute().get();
        } catch (InterruptedException ioe) {
            Log.e("RabbitMqConnection", "onCatalystInstanceDestroy", ioe);
        } catch (ExecutionException ee) {
            Log.e("RabbitMqConnection", "onCatalystInstanceDestroy", ee);
        }
    }

}
