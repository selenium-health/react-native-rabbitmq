import {NativeModules, NativeEventEmitter} from 'react-native';

const { EventEmitter } = NativeModules;

export class Exchange {

    constructor(connection, exchange_config) {

        this.callbacks = {};
        this.rabbitmqconnection = connection.rabbitmqconnection;

        this.name = exchange_config.name;
        this.exchange_config = exchange_config;

        const RabbitMqEmitter = new NativeEventEmitter(EventEmitter);

        this.subscription = RabbitMqEmitter.addListener('RabbitMqExchangeEvent', this.handleEvent.bind(this));

        this.rabbitmqconnection.addExchange(exchange_config);
    }

    handleEvent(event) {
        // eslint-disable-next-line no-prototype-builtins
        if (this.callbacks.hasOwnProperty(event.name)) {
            this.callbacks[event.name](event)
        }
    }

    on(event, callback) {
        this.callbacks[event] = callback;
    }

    removeon(event) {
        delete this.callbacks[event];
    }

    publish(message, routing_key = '', properties = {}, publish_id='') {
        this.rabbitmqconnection.publishToExchange(message, this.name, routing_key, properties, publish_id);
    }

    delete() {
        this.rabbitmqconnection.deleteExchange(this.name);
    }

}

export default Exchange;
