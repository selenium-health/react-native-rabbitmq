import {NativeModules, NativeEventEmitter} from 'react-native';

const { EventEmitter } = NativeModules;
const RabbitMqConnection = NativeModules.RabbitMqConnection;

export class Connection {    
    constructor(config) {
        this.config = config;
        this.rabbitmqconnection = RabbitMqConnection;
        this.callbacks = {};
        
        this.connected = false;

        const RabbitMqEmitter = new NativeEventEmitter(EventEmitter);

        this.subscription = RabbitMqEmitter.addListener('RabbitMqConnectionEvent', this.handleEvent.bind(this));

        this.rabbitmqconnection.initialize(config);
    }
    
    connect() {
        this.rabbitmqconnection.connect();
    }

    reconnect() {
        this.rabbitmqconnection.close();
        this.rabbitmqconnection.initialize(this.config);
        this.rabbitmqconnection.connect();
    }
    
    close() {
        this.rabbitmqconnection.close();
    }

    clear() {
        this.subscription.remove();
    }

    consumeAllQueues() {
        this.rabbitmqconnection.consumeAll()
    }

    handleEvent(event) {
        if (event.name === 'connected'){ this.connected = true; }

        // eslint-disable-next-line no-prototype-builtins
        if (this.callbacks.hasOwnProperty(event.name)){
            this.callbacks[event.name](event)
        }
    }
    
    on(event, callback) {
        this.callbacks[event] = callback;
    } 

    removeon(event) {
        delete this.callbacks[event];
    }
}

export default Connection;